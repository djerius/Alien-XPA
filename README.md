# NAME

Alien::XPA - Find or Build libxpa

# VERSION

version v2.1.20.8

# DESCRIPTION

This distribution installs the XPA library if its not available. It
provides a uniform interface via [Alien::Base](https://metacpan.org/pod/Alien%3A%3ABase) to configuration
information useful to link against it.

This module finds or builds version 2.1.20 of the `XPA` library,
which is bundled.

`XPA` is distributed under the MIT License.

For more information, please see [Alien::Build::Manual::AlienUser](https://metacpan.org/pod/Alien%3A%3ABuild%3A%3AManual%3A%3AAlienUser)

# USAGE

Please see [Alien::Build::Manual::AlienUser](https://metacpan.org/pod/Alien%3A%3ABuild%3A%3AManual%3A%3AAlienUser) (or equivalently on
metacpan
["/metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manua
l/AlienUser.pod" in https:](https://metacpan.org/pod/https%3A#metacpan.org-pod-distribution-Alien-Build-lib-Alien-Build-Manua-l-AlienUser.pod)).

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-alien-xpa@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-XPA](https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-XPA)

## Source

Source is available at

    https://gitlab.com/djerius/alien-xpa

and may be cloned from

    https://gitlab.com/djerius/alien-xpa.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2017 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
