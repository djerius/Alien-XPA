package Alien::XPA;

# ABSTRACT: Find or Build libxpa

use v5.10;
use strict;
use warnings;

our $VERSION = 'v2.1.20.8';

use base qw( Alien::Base );

1;

# COPYRIGHT

__END__

=for stopwords
metacpan

=head1 DESCRIPTION

This distribution installs the XPA library if its not available. It
provides a uniform interface via L<Alien::Base> to configuration
information useful to link against it.

This module finds or builds version 2.1.20 of the C<XPA> library,
which is bundled.

C<XPA> is distributed under the MIT License.

For more information, please see L<Alien::Build::Manual::AlienUser>

=head1 USAGE

Please see L<Alien::Build::Manual::AlienUser> (or equivalently on
metacpan
L<https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manua
l/AlienUser.pod>).

=cut
